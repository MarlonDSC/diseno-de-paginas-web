<?php include('header.php')?>

Representa la sangre derramada por los patriotas en las batallas para conseguir la independencia de la Nación.

Parte de un poema hacia la bandera:
El rojo de su gloriosa
decisión dice al oído,
Soy - dice - el laurel teñido
con su sangre generosa.
<?php include('footer.php')?>