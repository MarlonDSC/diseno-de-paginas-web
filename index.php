<!-- <!DOCTYPE html> -->
<!-- <html> -->

<!-- <head> -->
    <!--Import Google Icon Font-->
    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <!--Import materialize.css-->
    <!-- <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" /> -->
    <!-- <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" /> -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous"> -->

    <!--Let browser know website is optimized for mobile-->
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
        .col{
            height: 40%;
        }
    </style>
</head>

<body>
    <div class="nav">
        <ul class="nav navbar-nav">
            <li class="nav-item"><a href="index.php">Inicio</a></li>
            <li class="nav-item"><a href="azul.php">Azul</a></li>
            <li class="nav-item"><a href="rojo.php">Rojo</a></li>
            <li class="nav-item"><a href="blanco.php">Blanco</a></li>
            <li class="nav-item"><a href="escudo.php">Escudo</a></li>
        </ul>
    </div> -->
<?php include('header.php')?>
    <div class="container">
        <div class="row">
            <div class="col col-xs-5 label-primary"></div>
            <div class="col col-xs-2"></div>
            <div class="col col-xs-5 label-danger"></div>
        </div>
        <div class="row">
            <div class="col col-xs-5"></div>
            <div class="col col-xs-2 escudo">
                <img src="https://upload.wikimedia.org/wikipedia/commons/2/26/Coat_of_arms_of_the_Dominican_Republic.svg" alt="Girl in a jacket" width="100" height="100">
            </div>
            <div class="col col-xs-5"></div>
        </div>
        <div class="row">
            <div class="col col-xs-5 label-danger"></div>
            <div class="col col-xs-2"></div>
            <div class="col col-xs-5 label-primary"></div>
        </div>
    </div>
<?php include('footer.php')?>
<!-- </body>
</html> -->
